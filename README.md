# A Cova do Rei Cintolo

Juego de plataformas **2D** de estética **pixel art** desarrollado en la [**Ghrelo Jam!**](https://itch.io/jam/ghrelo-jam) 2022.

El objetivo será descender por una cueva evitando sus peligros mientras gestionas tus escasas fuentes de **luz**, las cuales te permitiran vislumbrar el camino más seguro. Los biomas de la cueva se volverán más agresivos conforme se desciende, incrementando la cantidad de obstáculos letales, trampas y con orografía más compleja.

Se busca la alta rejugabilidad con cuevas creadas proceduralmente y un componente de competitividad contigo mismo intentanto siempre superar tu mejor record.